package com.horovyi.ihor.cicd.bootstrap;

import com.horovyi.ihor.cicd.model.User;
import com.horovyi.ihor.cicd.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Created by ihor.horovyi 2019-05-17
 */
@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {


    private final UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {
        loadUsers();
    }

    private void loadUsers() {
        User ivan = User.builder().firstName("Ivan").lastName("Ivanov").build();
        userRepository.save(ivan);

        User anton = User.builder().firstName("Anton").lastName("Antonov").build();
        userRepository.save(anton);
    }

}
