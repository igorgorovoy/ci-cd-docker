package com.horovyi.ihor.cicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiCdDockerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CiCdDockerApplication.class, args);
    }

}
