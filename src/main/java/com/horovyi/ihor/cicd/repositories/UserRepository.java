package com.horovyi.ihor.cicd.repositories;

import com.horovyi.ihor.cicd.model.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by ihor.horovyi 2019-05-17
 */
public interface UserRepository extends CrudRepository<User, Long> {
}
