package com.horovyi.ihor.cicd.web;

import com.horovyi.ihor.cicd.model.User;
import com.horovyi.ihor.cicd.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ihor.horovyi 2019-05-17
 */
@Controller
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserRepository userRepository;

    @ResponseBody
    @GetMapping("/all")
    public ResponseEntity<Iterable<User>> allUsers() {
        return ResponseEntity.ok(userRepository.findAll());
    }

}
